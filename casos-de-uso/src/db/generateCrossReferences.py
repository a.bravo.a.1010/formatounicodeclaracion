"""
	Comando para generar las referencias cruzadas.
"""


#def generateCrossReferences():
import csv
import os
ar_MSGReference = open(os.getcwd() + '/db/cdtDBMSGReference.csv', 'r')
ar_BRReference = open(os.getcwd() + '/db/cdtDBBRReference.csv', 'r')
ar_ElementReference = open(os.getcwd() + '/db/cdtDBReference.csv', 'r')
ar_FinalReference = open(os.getcwd() + '/db/cdtAuxReference.csv', 'w+')

MSGReference = csv.reader(ar_MSGReference)
BRReference = csv.reader(ar_BRReference)
ElementReference = csv.reader(ar_ElementReference)

referencias = {}

for mensaje in MSGReference:
	if mensaje[0] not in referencias:
		referencias[mensaje[0]] = []
	if mensaje[1] not in referencias[mensaje[0]]:
		referencias[mensaje[0]].append(mensaje[1])

for regla in BRReference:
	if regla[0] not in referencias:
		referencias[regla[0]] = []
	if regla[1] not in referencias[regla[0]]:
		referencias[regla[0]].append(regla[1])

for elemento in ElementReference:
	if elemento[0] not in referencias:
		referencias[elemento[0]] = []
	if elemento[1] not in referencias[elemento[0]]:
		referencias[elemento[0]].append(elemento[1])


ar_FinalReference.write("idElemento,idElementoRef\n")
for key in referencias:
	ar_FinalReference.write(key + ", \cdtEmpty ")
	for value in referencias[key]:
		ar_FinalReference.write("\\refIdElem{" + value + "}\\\\ ")
	ar_FinalReference.write("\n")
