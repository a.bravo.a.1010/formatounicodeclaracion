\babel@toc {spanish}{}\relax 
\contentsline {chapter}{\numberline {1}Introducción}{1}{chapter.1}%
\contentsline {section}{\numberline {1.1}Intención del documento}{1}{section.1.1}%
\contentsline {section}{\numberline {1.2}Estructura del documento}{1}{section.1.2}%
\contentsline {chapter}{\numberline {2}Nomenclatura}{3}{chapter.2}%
\contentsline {section}{\numberline {2.1}Modelado del negocio}{3}{section.2.1}%
\contentsline {subsection}{\numberline {2.1.1}Modelado de procesos}{3}{subsection.2.1.1}%
\contentsline {subsection}{\numberline {2.1.2}Modelado de ciclos de vida de entidades}{3}{subsection.2.1.2}%
\contentsline {subsection}{\numberline {2.1.3}Modelado de reglas de negocio}{3}{subsection.2.1.3}%
\contentsline {subsection}{\numberline {2.1.4}Modelado de actores}{4}{subsection.2.1.4}%
\contentsline {section}{\numberline {2.2}Modelado de casos de uso}{4}{section.2.2}%
\contentsline {subsection}{\numberline {2.2.1}Descripción de casos de uso}{5}{subsection.2.2.1}%
\contentsline {section}{\numberline {2.3}Modelado de mensajes}{5}{section.2.3}%
\contentsline {part}{I\hspace {1em}Modelado del negocio}{7}{part.1}%
\contentsline {chapter}{\numberline {3}Modelo de estados}{9}{chapter.3}%
\contentsline {section}{\numberline {3.1}Modelo del ciclo de vida de la pregunta}{9}{section.3.1}%
\contentsline {subsection}{\numberline {3.1.1}Resumen}{9}{subsection.3.1.1}%
\contentsline {subsection}{\numberline {3.1.2}Descripción}{9}{subsection.3.1.2}%
\contentsline {section}{\numberline {3.2}Modelo del ciclo de vida del cuestionario de contexto}{11}{section.3.2}%
\contentsline {subsection}{\numberline {3.2.1}Resumen}{11}{subsection.3.2.1}%
\contentsline {subsection}{\numberline {3.2.2}Descripción}{11}{subsection.3.2.2}%
\contentsline {chapter}{\numberline {4}Reglas de negocio}{17}{chapter.4}%
\contentsline {section}{\numberline {4.1}Reglas de Negocio}{17}{section.4.1}%
\contentsline {subsection}{\numberline {4.1.1}{RN-N001} {Credenciales válidas para ingresar al sistema}}{17}{subsection.4.1.1}%
\contentsline {subsection}{\numberline {4.1.2}{RN-N002} {Realizar acción en una entidad asociada con otros elementos}}{18}{subsection.4.1.2}%
\contentsline {subsection}{\numberline {4.1.3}{RN-N003} {Unicidad de elementos registrados}}{18}{subsection.4.1.3}%
\contentsline {subsection}{\numberline {4.1.4}{RN-N004} {Rangos válidos para realizar una acción}}{19}{subsection.4.1.4}%
\contentsline {subsection}{\numberline {4.1.5}{RN-N005} {Fecha de inicio mayor o igual que la fecha actual}}{20}{subsection.4.1.5}%
\contentsline {subsection}{\numberline {4.1.6}{RN-N006} {Fecha de fin mayor o igual que la fecha de inicio.}}{20}{subsection.4.1.6}%
\contentsline {subsection}{\numberline {4.1.7}{RN-N007} {Elementos mínimos necesarios}}{21}{subsection.4.1.7}%
\contentsline {subsection}{\numberline {4.1.8}{RN-N008} {Permisos necesarios para realizar una acción en el sistema}}{21}{subsection.4.1.8}%
\contentsline {subsection}{\numberline {4.1.9}{RN-N009} {Relación de valores correcta}}{22}{subsection.4.1.9}%
\contentsline {subsection}{\numberline {4.1.10}{RN-N010} {Formato válido para un campo de texto numérico}}{23}{subsection.4.1.10}%
\contentsline {subsection}{\numberline {4.1.11}{RN-N011} {Formato válido para una fecha}}{23}{subsection.4.1.11}%
\contentsline {subsection}{\numberline {4.1.12}{RN-N012} {Formato válido para un correo electrónico}}{24}{subsection.4.1.12}%
\contentsline {subsection}{\numberline {4.1.13}{RN-N013} {Formato válido para la CURP}}{24}{subsection.4.1.13}%
\contentsline {subsection}{\numberline {4.1.14}{RN-N014} {Formato válido para una expresión regular}}{25}{subsection.4.1.14}%
\contentsline {subsection}{\numberline {4.1.15}{RN-N015} {Generar nombre de una entidad clonada}}{26}{subsection.4.1.15}%
\contentsline {subsection}{\numberline {4.1.16}{RN-N016} {Registrar histórico de la pregunta}}{27}{subsection.4.1.16}%
\contentsline {subsection}{\numberline {4.1.17}{RN-N018} {Tipos de preguntas que pueden condicionarse}}{27}{subsection.4.1.17}%
\contentsline {subsection}{\numberline {4.1.18}{RN-N026} {Formato y resolución de imágenes}}{29}{subsection.4.1.18}%
\contentsline {chapter}{\numberline {5}Reglas del sistema}{31}{chapter.5}%
\contentsline {section}{\numberline {5.1}Reglas del Sistema}{31}{section.5.1}%
\contentsline {subsection}{\numberline {5.1.1}{RN-S001} {Máquina de estados}}{31}{subsection.5.1.1}%
\contentsline {subsection}{\numberline {5.1.2}{RN-S002} {Campos obligatorios}}{32}{subsection.5.1.2}%
\contentsline {subsection}{\numberline {5.1.3}{RN-S003} {Formato de archivo}}{33}{subsection.5.1.3}%
\contentsline {subsection}{\numberline {5.1.4}{RN-S004} {Elementos registrados en el sistema}}{34}{subsection.5.1.4}%
\contentsline {chapter}{\numberline {6}Modelo de actores}{35}{chapter.6}%
\contentsline {section}{\numberline {6.1}Actor Elaborador senior}{35}{section.6.1}%
\contentsline {section}{\numberline {6.2}Actor Elaborador junior}{36}{section.6.2}%
\contentsline {section}{\numberline {6.3}Actor Revisor de contenido}{37}{section.6.3}%
\contentsline {section}{\numberline {6.4}Actor Revisor de estilo}{38}{section.6.4}%
\contentsline {section}{\numberline {6.5}Actor Elaborador auxiliar}{38}{section.6.5}%
\contentsline {section}{\numberline {6.6}Actor Administrador de actores}{39}{section.6.6}%
\contentsline {part}{II\hspace {1em}Modelado de Comportamiento}{41}{part.2}%
\contentsline {chapter}{\numberline {7}Módulo de Cuestionarios de Contexto}{43}{chapter.7}%
\contentsline {section}{\numberline {7.1}CC-CT-CU1 Gestionar cuestionarios de contexto}{44}{section.7.1}%
\contentsline {subsection}{\numberline {7.1.1}Resumen}{44}{subsection.7.1.1}%
\contentsline {subsection}{\numberline {7.1.2}Descripción}{44}{subsection.7.1.2}%
\contentsline {subsection}{\numberline {7.1.3}Trayectorias del Caso de Uso}{44}{subsection.7.1.3}%
\contentsline {subsection}{\numberline {7.1.4}Diseño de la interfaz}{49}{subsection.7.1.4}%
\contentsline {section}{\numberline {7.2}CC-CT-CU1.1 Registrar información básica del cuestionario de contexto}{61}{section.7.2}%
\contentsline {subsection}{\numberline {7.2.1}Resumen}{61}{subsection.7.2.1}%
\contentsline {subsection}{\numberline {7.2.2}Descripción}{61}{subsection.7.2.2}%
\contentsline {subsection}{\numberline {7.2.3}Trayectorias del Caso de Uso}{61}{subsection.7.2.3}%
\contentsline {subsection}{\numberline {7.2.4}Diseño de la interfaz}{63}{subsection.7.2.4}%
\contentsline {section}{\numberline {7.3}CC-CT-CU1.2 Editar información básica del cuestionario de contexto}{65}{section.7.3}%
\contentsline {subsection}{\numberline {7.3.1}Resumen}{65}{subsection.7.3.1}%
\contentsline {subsection}{\numberline {7.3.2}Descripción}{65}{subsection.7.3.2}%
\contentsline {subsection}{\numberline {7.3.3}Trayectorias del Caso de Uso}{65}{subsection.7.3.3}%
\contentsline {subsection}{\numberline {7.3.4}Diseño de la interfaz}{68}{subsection.7.3.4}%
\contentsline {section}{\numberline {7.4}CC-CT-CU1.3 Clonar cuestionario de contexto}{70}{section.7.4}%
\contentsline {subsection}{\numberline {7.4.1}Resumen}{70}{subsection.7.4.1}%
\contentsline {subsection}{\numberline {7.4.2}Descripción}{70}{subsection.7.4.2}%
\contentsline {subsection}{\numberline {7.4.3}Trayectorias del Caso de Uso}{70}{subsection.7.4.3}%
\contentsline {subsection}{\numberline {7.4.4}Diseño de la interfaz}{71}{subsection.7.4.4}%
\contentsline {section}{\numberline {7.5}CC-CT-CU1.4 Eliminar cuestionario de contexto}{73}{section.7.5}%
\contentsline {subsection}{\numberline {7.5.1}Resumen}{73}{subsection.7.5.1}%
\contentsline {subsection}{\numberline {7.5.2}Descripción}{73}{subsection.7.5.2}%
\contentsline {subsection}{\numberline {7.5.3}Trayectorias del Caso de Uso}{73}{subsection.7.5.3}%
\contentsline {subsection}{\numberline {7.5.4}Diseño de la interfaz}{74}{subsection.7.5.4}%
\contentsline {section}{\numberline {7.6}CC-CT-CU2 Generar cuestionario de contexto}{76}{section.7.6}%
\contentsline {subsection}{\numberline {7.6.1}Resumen}{76}{subsection.7.6.1}%
\contentsline {subsection}{\numberline {7.6.2}Descripción}{76}{subsection.7.6.2}%
\contentsline {subsection}{\numberline {7.6.3}Trayectorias del Caso de Uso}{76}{subsection.7.6.3}%
\contentsline {subsection}{\numberline {7.6.4}Diseño de la interfaz}{80}{subsection.7.6.4}%
\contentsline {section}{\numberline {7.7}CC-CT-CU2.1 Agregar sección al cuestionario de contexto}{86}{section.7.7}%
\contentsline {subsection}{\numberline {7.7.1}Resumen}{86}{subsection.7.7.1}%
\contentsline {subsection}{\numberline {7.7.2}Descripción}{86}{subsection.7.7.2}%
\contentsline {subsection}{\numberline {7.7.3}Trayectorias del Caso de Uso}{86}{subsection.7.7.3}%
\contentsline {subsection}{\numberline {7.7.4}Diseño de la interfaz}{88}{subsection.7.7.4}%
\contentsline {section}{\numberline {7.8}CC-CT-CU2.2 Agregar preguntas a la sección del cuestionario de contexto}{89}{section.7.8}%
\contentsline {subsection}{\numberline {7.8.1}Resumen}{89}{subsection.7.8.1}%
\contentsline {subsection}{\numberline {7.8.2}Descripción}{89}{subsection.7.8.2}%
\contentsline {subsection}{\numberline {7.8.3}Trayectorias del Caso de Uso}{89}{subsection.7.8.3}%
\contentsline {subsection}{\numberline {7.8.4}Diseño de la interfaz}{92}{subsection.7.8.4}%
\contentsline {section}{\numberline {7.9}CC-CT-CU2.3 Seleccionar sinonimia de la pregunta}{97}{section.7.9}%
\contentsline {subsection}{\numberline {7.9.1}Resumen}{97}{subsection.7.9.1}%
\contentsline {subsection}{\numberline {7.9.2}Descripción}{97}{subsection.7.9.2}%
\contentsline {subsection}{\numberline {7.9.3}Trayectorias del Caso de Uso}{97}{subsection.7.9.3}%
\contentsline {subsection}{\numberline {7.9.4}Diseño de la interfaz}{98}{subsection.7.9.4}%
\contentsline {section}{\numberline {7.10}CC-CT-CU2.4 Editar sección del cuestionario de contexto}{100}{section.7.10}%
\contentsline {subsection}{\numberline {7.10.1}Resumen}{100}{subsection.7.10.1}%
\contentsline {subsection}{\numberline {7.10.2}Descripción}{100}{subsection.7.10.2}%
\contentsline {subsection}{\numberline {7.10.3}Trayectorias del Caso de Uso}{100}{subsection.7.10.3}%
\contentsline {subsection}{\numberline {7.10.4}Diseño de la interfaz}{102}{subsection.7.10.4}%
\contentsline {section}{\numberline {7.11}CC-CT-CU2.5 Eliminar sección del cuestionario de contexto}{103}{section.7.11}%
\contentsline {subsection}{\numberline {7.11.1}Resumen}{103}{subsection.7.11.1}%
\contentsline {subsection}{\numberline {7.11.2}Descripción}{103}{subsection.7.11.2}%
\contentsline {subsection}{\numberline {7.11.3}Trayectorias del Caso de Uso}{103}{subsection.7.11.3}%
\contentsline {subsection}{\numberline {7.11.4}Diseño de la interfaz}{104}{subsection.7.11.4}%
\contentsline {section}{\numberline {7.12}CC-CT-CU2.6 Condicionar pregunta del cuestionario de contexto}{106}{section.7.12}%
\contentsline {subsection}{\numberline {7.12.1}Resumen}{106}{subsection.7.12.1}%
\contentsline {subsection}{\numberline {7.12.2}Descripción}{106}{subsection.7.12.2}%
\contentsline {subsection}{\numberline {7.12.3}Trayectorias del Caso de Uso}{106}{subsection.7.12.3}%
\contentsline {subsection}{\numberline {7.12.4}Diseño de la interfaz}{109}{subsection.7.12.4}%
\contentsline {section}{\numberline {7.13}CC-CT-CU2.8 Eliminar preguntas de la sección del cuestionario de contexto}{115}{section.7.13}%
\contentsline {subsection}{\numberline {7.13.1}Resumen}{115}{subsection.7.13.1}%
\contentsline {subsection}{\numberline {7.13.2}Descripción}{115}{subsection.7.13.2}%
\contentsline {subsection}{\numberline {7.13.3}Trayectorias del Caso de Uso}{115}{subsection.7.13.3}%
\contentsline {subsection}{\numberline {7.13.4}Diseño de la interfaz}{116}{subsection.7.13.4}%
\contentsline {section}{\numberline {7.14}CC-CT-CU2.9 Eliminar regla a la respuesta de la pregunta}{118}{section.7.14}%
\contentsline {subsection}{\numberline {7.14.1}Resumen}{118}{subsection.7.14.1}%
\contentsline {subsection}{\numberline {7.14.2}Descripción}{118}{subsection.7.14.2}%
\contentsline {subsection}{\numberline {7.14.3}Trayectorias del Caso de Uso}{118}{subsection.7.14.3}%
\contentsline {subsection}{\numberline {7.14.4}Diseño de la interfaz}{119}{subsection.7.14.4}%
\contentsline {section}{\numberline {7.15}CC-CT-CU3 Consultar cuestionario de contexto}{120}{section.7.15}%
\contentsline {subsection}{\numberline {7.15.1}Resumen}{120}{subsection.7.15.1}%
\contentsline {subsection}{\numberline {7.15.2}Descripción}{120}{subsection.7.15.2}%
\contentsline {subsection}{\numberline {7.15.3}Trayectorias del Caso de Uso}{120}{subsection.7.15.3}%
\contentsline {subsection}{\numberline {7.15.4}Diseño de la interfaz}{132}{subsection.7.15.4}%
\contentsline {section}{\numberline {7.16}CC-CT-CU4 Enviar cuestionario de contexto a revisión de contenido}{133}{section.7.16}%
\contentsline {subsection}{\numberline {7.16.1}Resumen}{133}{subsection.7.16.1}%
\contentsline {subsection}{\numberline {7.16.2}Descripción}{133}{subsection.7.16.2}%
\contentsline {subsection}{\numberline {7.16.3}Trayectorias del Caso de Uso}{133}{subsection.7.16.3}%
\contentsline {subsection}{\numberline {7.16.4}Diseño de la interfaz}{134}{subsection.7.16.4}%
\contentsline {section}{\numberline {7.17}CC-CT-CU5 Validar observaciones del contenido}{136}{section.7.17}%
\contentsline {subsection}{\numberline {7.17.1}Resumen}{136}{subsection.7.17.1}%
\contentsline {subsection}{\numberline {7.17.2}Descripción}{136}{subsection.7.17.2}%
\contentsline {subsection}{\numberline {7.17.3}Trayectorias del Caso de Uso}{136}{subsection.7.17.3}%
\contentsline {subsection}{\numberline {7.17.4}Diseño de la interfaz}{138}{subsection.7.17.4}%
\contentsline {section}{\numberline {7.18}CC-CT-CU5.1 Consultar observaciones de la pregunta}{143}{section.7.18}%
\contentsline {subsection}{\numberline {7.18.1}Resumen}{143}{subsection.7.18.1}%
\contentsline {subsection}{\numberline {7.18.2}Descripción}{143}{subsection.7.18.2}%
\contentsline {subsection}{\numberline {7.18.3}Trayectorias del Caso de Uso}{143}{subsection.7.18.3}%
\contentsline {subsection}{\numberline {7.18.4}Diseño de la interfaz}{143}{subsection.7.18.4}%
\contentsline {section}{\numberline {7.19}CC-CT-CU5.2 Registrar respuesta a la observacion}{145}{section.7.19}%
\contentsline {subsection}{\numberline {7.19.1}Resumen}{145}{subsection.7.19.1}%
\contentsline {subsection}{\numberline {7.19.2}Descripción}{145}{subsection.7.19.2}%
\contentsline {subsection}{\numberline {7.19.3}Trayectorias del Caso de Uso}{145}{subsection.7.19.3}%
\contentsline {subsection}{\numberline {7.19.4}Diseño de la interfaz}{146}{subsection.7.19.4}%
\contentsline {section}{\numberline {7.20}CC-CT-CU5.3 Editar respuesta registrada}{148}{section.7.20}%
\contentsline {subsection}{\numberline {7.20.1}Resumen}{148}{subsection.7.20.1}%
\contentsline {subsection}{\numberline {7.20.2}Descripción}{148}{subsection.7.20.2}%
\contentsline {subsection}{\numberline {7.20.3}Trayectorias del Caso de Uso}{148}{subsection.7.20.3}%
\contentsline {subsection}{\numberline {7.20.4}Diseño de la interfaz}{149}{subsection.7.20.4}%
\contentsline {section}{\numberline {7.21}CC-CT-CU5.4 Eliminar respuesta de las observaciones}{151}{section.7.21}%
\contentsline {subsection}{\numberline {7.21.1}Resumen}{151}{subsection.7.21.1}%
\contentsline {subsection}{\numberline {7.21.2}Descripción}{151}{subsection.7.21.2}%
\contentsline {subsection}{\numberline {7.21.3}Trayectorias del Caso de Uso}{151}{subsection.7.21.3}%
\contentsline {subsection}{\numberline {7.21.4}Diseño de la interfaz}{152}{subsection.7.21.4}%
\contentsline {section}{\numberline {7.22}CC-CT-CU5.5 Habilitar edición del cuestionario de contexto}{154}{section.7.22}%
\contentsline {subsection}{\numberline {7.22.1}Resumen}{154}{subsection.7.22.1}%
\contentsline {subsection}{\numberline {7.22.2}Descripción}{154}{subsection.7.22.2}%
\contentsline {subsection}{\numberline {7.22.3}Trayectorias del Caso de Uso}{154}{subsection.7.22.3}%
\contentsline {subsection}{\numberline {7.22.4}Diseño de la interfaz}{155}{subsection.7.22.4}%
\contentsline {section}{\numberline {7.23}CC-CT-CU5.6 Aprobar contenido del cuestionario de contexto}{157}{section.7.23}%
\contentsline {subsection}{\numberline {7.23.1}Resumen}{157}{subsection.7.23.1}%
\contentsline {subsection}{\numberline {7.23.2}Descripción}{157}{subsection.7.23.2}%
\contentsline {subsection}{\numberline {7.23.3}Trayectorias del Caso de Uso}{157}{subsection.7.23.3}%
\contentsline {subsection}{\numberline {7.23.4}Diseño de la interfaz}{158}{subsection.7.23.4}%
\contentsline {section}{\numberline {7.24}CC-CT-CU6 Aprobar cuestionario de contexto}{160}{section.7.24}%
\contentsline {subsection}{\numberline {7.24.1}Resumen}{160}{subsection.7.24.1}%
\contentsline {subsection}{\numberline {7.24.2}Descripción}{160}{subsection.7.24.2}%
\contentsline {subsection}{\numberline {7.24.3}Trayectorias del Caso de Uso}{160}{subsection.7.24.3}%
\contentsline {subsection}{\numberline {7.24.4}Diseño de la interfaz}{161}{subsection.7.24.4}%
\contentsline {part}{III\hspace {1em}Modelado de Interacción}{165}{part.3}%
\contentsline {chapter}{\numberline {8}Menús del sistema}{169}{chapter.8}%
\contentsline {chapter}{\numberline {9}Mensajes del sistema}{171}{chapter.9}%
\contentsline {section}{\numberline {9.1}Mensajes a través de la pantalla}{171}{section.9.1}%
\contentsline {subsection}{\numberline {9.1.1}{MSG1}~{}{Campos obligatorios}}{171}{subsection.9.1.1}%
\contentsline {subsection}{\numberline {9.1.2}{MSG2}~{}{Duplicidad de información}}{172}{subsection.9.1.2}%
\contentsline {subsection}{\numberline {9.1.3}{MSG3}~{}{Operación exitosa}}{172}{subsection.9.1.3}%
\contentsline {subsection}{\numberline {9.1.4}{MSG4}~{}{Ingresar datos válidos}}{173}{subsection.9.1.4}%
\contentsline {subsection}{\numberline {9.1.5}{MSG5}~{}{Valor fuera del rango}}{173}{subsection.9.1.5}%
\contentsline {subsection}{\numberline {9.1.6}{MSG6}~{}{Relación de valores incorrecta}}{173}{subsection.9.1.6}%
\contentsline {subsection}{\numberline {9.1.7}{MSG7}~{}{No se encontraron resultados en la búsqueda}}{174}{subsection.9.1.7}%
\contentsline {subsection}{\numberline {9.1.8}{MSG9}~{}{Elementos mínimos necesarios}}{174}{subsection.9.1.8}%
\contentsline {subsection}{\numberline {9.1.9}{MSG10}~{}{Impedimento de acción}}{174}{subsection.9.1.9}%
\contentsline {subsection}{\numberline {9.1.10}{MSG11}~{}{Estado no permitido}}{175}{subsection.9.1.10}%
\contentsline {subsection}{\numberline {9.1.11}{MSG12}~{}{No tiene permisos suficientes}}{175}{subsection.9.1.11}%
\contentsline {subsection}{\numberline {9.1.12}{MSG13}~{}{Formato de archivo no válido}}{175}{subsection.9.1.12}%
\contentsline {subsection}{\numberline {9.1.13}{MSG14}~{}{Cuestionario de contexto sin información registrada}}{175}{subsection.9.1.13}%
